<?php

namespace App\Form;

use App\Entity\Tag;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class CreateArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, ['label' => false, 'attr' => ['placeholder' => 'Введите тему статьи', 'class' => 'title_form']])
            ->add('date', DateType::class, ['widget' => 'choice', 'label' => false, 'attr' => ['class' => 'datepicker']])
            ->add('tags', EntityType::class,
                ['attr' => ['class' => 'tags_form'],
                    'label' => 'Выберите теги',
                    'multiple' => true,
                    'expanded' => true,
                    'placeholder' => 'Выберите тег',
                    'class' => Tag::class,
                    'choice_label' => 'title',
                ])
            ->add('description', TextareaType::class, ['label' => false, 'attr' => ['placeholder' => 'Введите текст статьи']])
            ->add('submit', SubmitType::class, ['label' => "Создать статью", 'attr' => ['class' => 'btn btn-primary']]);
    }
}