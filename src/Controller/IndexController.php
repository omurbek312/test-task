<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Tag;
use App\Form\CreateArticleType;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{
    /**
     * @Route("/",name="indexPage")
     *
     * @param ObjectManager $em
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(ObjectManager $em)
    {
        $articles = $em->getRepository('App:Article')->findAll();
        $tags = $em->getRepository('App:Tag')->findAll();
        return $this->render('index.html.twig',
            ['articles' => $articles, 'tags' => $tags]);
    }

    /**
     * @Route("/filter_for_tags/{id}",name="filter")
     * @param ObjectManager $em
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function filterArticles(ObjectManager $em, int $id)
    {
        $articles = $em->getRepository('App:Article')->tagFilter($id);
        $tags = $em->getRepository('App:Tag')->findAll();
        $tag_for_filter = $em->getRepository('App:Tag')->find($id);
        return $this->render('filter_Article.html.twig', ['articles' => $articles, 'tags' => $tags, 'filter_tag' => $tag_for_filter]);
    }

    /**
     * @Route("/createArticle",name="CreateArticle")
     * @Method("Post")
     * @param Request $request
     * @param ObjectManager $em
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function CreateArticle(Request $request, ObjectManager $em)
    {
        $article = new Article();
        $form = $this->createForm(CreateArticleType::class, $article);
        dump($request->request);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($article);
            $em->flush();
            return $this->redirectToRoute('indexPage');
        }

        return $this->render("CreateArticle.html.twig",
            [
                'form' => $form->createView()
            ]);
    }

    /**
     * @Route("/createTag",name="createTag")
     * @param ObjectManager $em
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createTag(ObjectManager $em, Request $request)
    {
        $tag = new Tag();
        $form = $this->createForm(\App\Form\Tag::class, $tag);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($tag);
            $em->flush();
            return $this->redirectToRoute('indexPage');
        }
        return $this->render('createTag.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/article/{id}",name="article")
     * @param ObjectManager $em
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ArticlePage(ObjectManager $em, int $id)
    {
        $article = $em->getRepository('App:Article')->find($id);
        return $this->render('article.html.twig',['article'=>$article]);
    }

}