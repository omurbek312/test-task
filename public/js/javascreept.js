$(document).ready(function () {
    var readeMoreHtml = $('.read_more').html().trim();
    var lessText = readeMoreHtml.substr(0, 40);
    if (readeMoreHtml.length > 40) {
        $('.read_more').html(lessText).append("<a href='' class='read_more_link'>Читать больше ...<a>")
    } else {
        $('.read_more').html(readeMoreHtml)
    }
    $('body').on('click','.read_more_link',function (event) {
        event.preventDefault();
        $(this).parent(".read_more").html(readeMoreHtml).append("<a href='' class='show_less_link'>Скрыть</a>")
    })
    $('body').on('click','.show_less_link',function (event) {
        event.preventDefault();
        $(this).parent(".read_more").html(readeMoreHtml.substr(0,40)).append("<a href='' class='read_more_link'>Читать далее ...</a>")
    })
});